export const littleCameraBackground = "#E4E5EA";
export const blueColor = "#3872DC";
export const bigBlackColor = "#1E1E1E";
export const blackColor = "#050505";
export const iconColor = "#8A8D90";