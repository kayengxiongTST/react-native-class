import React from "react";
import { View, Text, Image, Modal, TouchableOpacity } from "react-native";
import { Ionicons, MaterialIcons, AntDesign } from "@expo/vector-icons";
import hueImage from "./assets/profile.jpeg";
import DayComponent from "./components/DayComponent";
import TimeComponent from "./components/TimeComponent";
import ActivityComponent from "./components/ActivityComponent";
import HorizontalLineComponent from "./components/HorizontalLineComponent";
import { MAIN_COLOR } from "./constants/colors";


let data = [
  {
    title: "Mobile by React",
    subtitle: "Nike and Adidas",
    time: "9 AM - 10 AM",
    images: {
      image1: '',
      image2: ''
    }
  },
  {
    title: "Software Engineer",
    subtitle: "Shoes and Sneakers",
    time: "11 AM - 12 AM",
    images: {
      image1: '',
      image2: ''
    }
  }
]
const App = () => {
  const [showModal, setShowModal] = React.useState(false);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: "#DBE9F7",
        padding: 20,
        opacity: showModal ? 0.5 : 1
      }}
    >
      <Modal
        visible={showModal}
        transparent={true}
        animationType="slide"
      >
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <View style={{
            width: 300,
            height: 200,
            borderRadius: 20,
            backgroundColor: '#09F',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.00,

            elevation: 24,
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <TouchableOpacity style={{
              backgroundColor: "#FFF",
              padding: 20,
              width: 200,
              borderRadius: 10
            }} onPress={() => setShowModal(false)}>
              <Text style={{
                color: "#09F",
                textAlign: 'center'
              }}>Close This Modal</Text>
            </TouchableOpacity>

          </View>
        </View>
      </Modal>
      <View
        style={{
          flex: 1,
          paddingTop: 20,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              borderWidth: 2,
              borderColor: "#A5BCDA",
              width: 50,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 10,
            }}
          >
            <Ionicons name="arrow-back-outline" size={30} color="#495D88" />
          </View>
          <Image
            source={require('./assets/cover.jpeg')}
            resizeMode="cover"
            style={{
              backgroundColor: "red",
              width: 40,
              height: 40,
              borderRadius: 100,
              borderWidth: 2,
              borderColor: "#ffffff",
            }}
          />
        </View>
        <View
          style={{
            padding: 30,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <Ionicons name="arrow-back-outline" size={30} color="#495D88" />
            <Text
              style={{
                paddingTop: 5,
              }}
            >
              Mar
            </Text>
          </View>
          <Text
            style={{
              fontSize: 30,
              fontWeight: "bold",
              color: MAIN_COLOR,
              bottom: 10,
            }}
            onPress={() => setShowModal(true)}
          >
            April
          </Text>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <Text
              style={{
                paddingTop: 5,
              }}
            >
              May
            </Text>
            <Ionicons name="arrow-forward-outline" size={30} color="#495D88" />
          </View>
        </View>
        <View
          style={{
            padding: 4,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <DayComponent active={true} number={12} title="Wed" />
          <DayComponent active={false} number={13} title="Thu" />
          <DayComponent active={false} number={14} title="Fri" />
          <DayComponent active={false} number={15} title="Sat" />
          <DayComponent active={false} number={16} title="Sun" />
        </View>
      </View>

      <View
        style={{
          flex: 2,
          paddingTop: 20,
        }}
      >
        <Text
          style={{
            fontSize: 30,
            fontWeight: "bold",
            color: MAIN_COLOR,
          }}
        >
          Ongoing
        </Text>

        <View
          style={{
            flex: 1,
            flexDirection: "row",
            paddingTop: 20,
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
            }}
          >
            <TimeComponent time={9} text='AM' />
            <TimeComponent time={10} text='AM' />
            <TimeComponent time={11} text='AM' />
            <TimeComponent time={12} text='AM' />
            <TimeComponent time={1} text='PM' />
            <TimeComponent time={2} text='PM' />
            <TimeComponent time={3} text='PM' />
          </View>

          <View
            style={{
              flex: 3,
            }}
          >
            <ActivityComponent title="Mobile App Design" subtitle="Mike and Anita" time="9 AM - 10 AM" />
            <HorizontalLineComponent />
            {
              data.map((data, index) => {
                return (
                  <View key={index}>
                    <ActivityComponent title={data.title} subtitle={data.subtitle} time={data.time} />
                    <View
                      style={{
                        padding: 10,
                      }}
                    ></View>
                  </View>
                )
              })
            }
          </View>
        </View>
      </View>
    </View>
  );
};

export default App;