import React from 'react'
import { View, Text } from 'react-native'

export default function TimeComponent({ time, text }) {
    return (
        <Text
            style={{
                fontSize: 18,
                color: "#60708C",
                paddingTop: 35,
            }}
        >
            {time} {text}
        </Text>
    )
}
