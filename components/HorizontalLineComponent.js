import React from 'react'
import { View } from 'react-native'

export default function HorizontalLineComponent() {
    return (
        <View
            style={{
                padding: 20,
            }}
        >
            <View>
                <View
                    style={{
                        width: 20,
                        height: 20,
                        backgroundColor: "red",
                        borderRadius: 10,
                        borderWidth: 5,
                        borderColor: "#fff",
                        right: 30,
                    }}
                />
                <View
                    style={{
                        height: 2,
                        width: 260,
                        top: 10,
                        left: -4,
                        backgroundColor: "#D0ACBD",
                        position: "absolute",
                    }}
                />
            </View>
        </View>
    )
}
