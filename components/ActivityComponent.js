import React from 'react'
import { View, Text, Image } from 'react-native'
import LoveImage from "../assets/cover.jpeg";
import { SECONDARY_COLOR } from '../constants/colors';
import ImageComponent from './ImageComponent';

export default function ActivityComponent({ title, subtitle, time }) {
    return (
        <View
            style={{
                width: 270,
                height: 120,
                backgroundColor: SECONDARY_COLOR,
                borderRadius: 20,
                padding: 13,
            }}
        >
            <Text
                style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: "#ffffff",
                }}
            >
                {title}
            </Text>
            <Text
                style={{
                    color: "#959DFA",
                }}
            >
                {subtitle}
            </Text>
            <View style={{ padding: 4 }}></View>
            <View
                style={{
                    flexDirection: "row",
                }}
            >
                <View>
                    <ImageComponent image={'https://media1.popsugar-assets.com/files/thumbor/hnVKqXE-xPM5bi3w8RQLqFCDw_E/475x60:1974x1559/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2019/09/09/023/n/1922398/9f849ffa5d76e13d154137.01128738_/i/Taylor-Swift.jpg'}/>
                    <ImageComponent left={30} image={'https://variety.com/wp-content/uploads/2020/01/taylor-swift-variety-cover-5-16x9-1000.jpg?w=681&h=383&crop=1'} />
                </View>
                <Text
                    style={{
                        fontSize: 18,
                        paddingTop: 5,
                        color: "#fff",
                        left: 130,
                    }}
                >
                    {" "}
                    {time}
                </Text>
            </View>
        </View>
    )
}
