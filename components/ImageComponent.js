import React from 'react'
import { Image } from 'react-native'

export default function ImageComponent({ left, image }) {
    return (
        <Image
            source={{
                uri: image,
            }}
            style={{
                backgroundColor: "blue",
                width: 40,
                height: 40,
                borderRadius: 100,
                position: "absolute",
                borderWidth: 2,
                borderColor: "#fff",
                left: left
            }}
        />
    )
}
