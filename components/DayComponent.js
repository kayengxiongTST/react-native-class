import React from 'react'
import { View, Text } from 'react-native'

export default function DayComponent({ active, number, title }) {
    return (
        <>
            <View
                style={{
                    width: 60,
                    height: 100,
                    backgroundColor: active === true ? "#412DA8" : "#F0F6FB",
                    borderRadius: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 5,
                    },
                    shadowOpacity: 0.34,
                    shadowRadius: 6.27,

                    elevation: 10,
                }}
            >
                <Text
                    style={{
                        fontWeight: "bold",
                        fontSize: 18,
                        color: active === true ? "#ffffff" : "#412DA8",
                    }}
                >
                    {number}
                </Text>
                <Text
                    style={{
                        color: active === true ? "#ffffff" : "#412DA8",
                    }}
                >
                    {title}
                </Text>
            </View>
        </>
    )
}
